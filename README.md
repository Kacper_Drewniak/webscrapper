# TypeScript WebScrapper | 2020

![logo](https://raw.githubusercontent.com/KacperDrewniak/trashes/master/logo.png)

To start application.

```bash
npm run start:dev
```

Create new scrap

```javascript
import Scrapper from "./modules/Scrapper";

const myScrap = new Scrapper(
    "https://eune.op.gg/ranking/ladder/page=", //url
     10,                                       //number of pages
     [{                                        //properties array
          name: "rank",             
          selector: '.ranking-table__cell--rank',
      }, {
          name: "summoner",
          selector: '.ranking-table__cell--summoner a span',
      }],
     ".ranking-table__row",                    //row selector
     "gg-data",                                //page title 
);

myScrap.scrapWeb();
```

