import Scrapper from "./modules/Scrapper";
import {RecordInterface} from "./interfaces/interfaces";

const propertiesOfOpGG: RecordInterface[] = [{
    name: "rank",
    selector: '.ranking-table__cell--rank',
}, {
    name: "summoner",
    selector: '.ranking-table__cell--summoner a span',
},
    {
        name: "tier",
        selector: '.ranking-table__cell--tier',
    }, {
        name: "lp",
        selector: '.ranking-table__cell--lp',
    },
    {
        name: "winRatio",
        selector: '.winratio-graph__text--left',
    }, {
        name: "loseRatio",
        selector: '.winratio-graph__text--right',
    },
]

const propertiesOfPracuj: RecordInterface[] = [{
    name: "offer-name",
    selector: '.offer-details__title-link',
}, {
    name: "location",
    selector: '.offer-labels__item',
}, {
    name: "company",
    selector: '.offer-company__name',
},]

const op_gg_scrap = new Scrapper(
    "https://eune.op.gg/ranking/ladder/page=",
    5,
    propertiesOfOpGG,
    ".ranking-table__row",
    "gg-data",
);


const pracuj_scrap = new Scrapper(
    'https://www.pracuj.pl/praca?pn=',
    10,
    propertiesOfPracuj,
    ".offer__info",
    "pracuj"
);

op_gg_scrap.scrapWeb();
pracuj_scrap.scrapWeb();



