export interface XslInterface{
    header: string;
    key: string;
}

export interface RecordInterface {
    name: string;
    selector: string;
    value?: string;
}
