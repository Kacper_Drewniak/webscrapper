import {getContent, getCreateJson, getCreateXsl, getHtml} from "../helpers/helpers"
import {RecordInterface} from "../interfaces/interfaces"

const cheerio = require('cheerio');

class Scrapper {
    scrapped: object[] = [];
    properties: RecordInterface[];
    pages: number;
    url: string;
    selectorRowTable: string;
    pageName: string;
    outputDir: string;

    constructor(url: string,
                pages: number = 1,
                properties: RecordInterface[],
                selectorRowTable: string,
                pageName: string,
                outputDir: string = "./") {
        this.pages = pages;
        this.url = url;
        this.properties = properties;
        this.selectorRowTable = selectorRowTable;
        this.pageName = pageName;
        this.outputDir = outputDir;
    }

    async scrapWeb(): Promise<void> {

        let scrapped : object[] = [];

        for (let i = 1; i <= this.pages; i++) {
            console.log(this.url + i )
            const $ = cheerio.load(await getHtml(this.url, i));
            $(this.selectorRowTable).each((i: number, el: any) => {
                const data: object = {};
                this.properties.forEach(({name, selector}) => {
                    // @ts-ignore
                    data[name] = getContent($, el, selector);
                })
                scrapped.push(data);
            })
        }

        this.scrapped = scrapped;

        if (this.scrapped.length) {
            this.fromScrapToExcel();
            this.fromScrapToJson();
        }
    }

    fromScrapToExcel():void {
        getCreateXsl(this.pageName, this.scrapped, this.properties,this.outputDir);
    }

    fromScrapToJson():void {
        getCreateJson(this.pageName, this.scrapped,this.outputDir);
    }

}

export default Scrapper;