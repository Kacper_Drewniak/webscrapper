import {RecordInterface,XslInterface} from "../interfaces/interfaces"

const fetch = require('isomorphic-fetch')
const fs = require('fs');
const colors = require('colors/safe')
const Excel = require('exceljs');


export const getContent = (dollar: any, el: object, sel: string): string => {
    return dollar(el).find(sel).text().replace(/\n\s+\t\s+/g, '');
}

export const getHtml = async (url: string, nr: number): Promise<void> => {
    const response = await fetch(`${url}${nr}`);
    return await response.text();
}

export const getNameCapitalized = (text: string): string => {
    return text.charAt(0).toUpperCase() + text.slice(1);
}

export const getCreateJson = (pageName:string,dataJson:object[],outputDir:string):void => {
    const data = JSON.stringify(dataJson);
    fs.writeFile(`${outputDir}${pageName}.json`, data, (err: object) => {
        console.log(colors.bgBlack.magenta(`Save file ${pageName}.json`));
        if (err) throw err;
    });
}

export const getCreateXsl = (pageName:string,dataJson:object[],properties:RecordInterface[],outputDir:string):void =>{
    const workbook = new Excel.Workbook();
    const worksheet = workbook.addWorksheet('ExampleSheet');

    let columns:XslInterface[] = [];

    properties.forEach(({name}) => {
        columns.push({
            header: getNameCapitalized(name),
            key: name
        })
    })

    worksheet.columns = columns;

    if (dataJson.length) {
        dataJson.forEach((player: object) => {
            worksheet.addRow(player);
        });
        workbook
            .xlsx
            .writeFile(`${outputDir}${pageName}.xlsx`)
            .then(()=> console.log(colors.bgBlack.magenta(`Save file ${pageName}.xlsx`)))
            .catch((err: object) => console.log("err", err));
    }
}

